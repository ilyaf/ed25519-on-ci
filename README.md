CI separates multiline environmental variables with CRLF (`\r\n`) and not LF (`\n`).
This breaks ed25519 keys pasted as a secret variable. Even if keys are pasted from Linux machines where there is no CRLF in input, the environmental variables still contain them in pipeline.
This also introduces the case where "local docker works fine" but "CI docker fails on the same exact key", which is PITA to debug for the first time.

This repo is the illustration of this behavior.

## Notes ##

Test private key generated with `ssh-keygen -t ed25519 -f test_key` added to this project's CI settings under the name of `SSH_PRIVATE_KEY` for `after_script` to fail.
