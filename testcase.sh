#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is skeleton template for new .sh files
set -eufo pipefail
IFS=$'\t\n'

D="$(cd "$(dirname "$0")"; pwd)"
cd "${D}"

test -n "${SSH_AUTH_SOCK}" || eval $(ssh-agent -s)

echo "Generate RSA keys, one normal (LF), one with CRLF separators"
ssh-keygen -N '' -C test-rsa-key-lf -f test_rsa_lf -b 1024 -t rsa
ssh-keygen -N '' -C test-rsa-key-crlf -f test_rsa_crlf -b 1024 -t rsa
unix2dos test_rsa_crlf

echo "hexdump both for comparison"
echo "with LF: "
hexdump -C test_rsa_lf
echo "with CRLF: "
hexdump -C test_rsa_crlf

echo "Make sure they are both added ok, because openssl handles them"
for f in test_rsa_lf test_rsa_crlf; do
	ssh-add "${f}" && ssh-add -d "${f}"
done

echo "Now, do same for Edwards curve keys"
ssh-keygen -N '' -C test-edwards-key-lf -f test_edwards_lf -t ed25519
ssh-keygen -N '' -C test-edwards-key-crlf -f test_edwards_crlf -t ed25519
unix2dos test_edwards_crlf

echo "hexdump both for comparison"
echo "with LF: "
hexdump -C test_edwards_lf
echo "with CRLF: "
hexdump -C test_edwards_crlf

echo "Check loading:"
echo "with LF should work:"
ssh-add test_edwards_lf && ssh-add -d test_edwards_lf
echo "with CRLF should ask for passphrase and fail:"
ssh-add test_edwards_crlf && ssh-add -d test_edwards_crlf

